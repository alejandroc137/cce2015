# IV Ciclo de Cursos Especiales en Sistemas Dinámicos y Computación Científica

##<== En el link "Source" está todo el material del Curso

El Programa de Matemáticas presenta la cuarta versión de este ciclo de cursos con una duración total de 40 horas, dirigido a estudiantes e investigadores en las áreas de Matemática, Física, Estadística y Astronomía. En esta ocasión, los Doctores Santiado Velasco, Carlos Piedrahíta, Alexander Arredondo y Manuel Balaguera, tratarán temáticas de los sistemas dinámicos como los grafos para análsis de imágenes, la propagación de ondas sísmicas, los sistemas hamiltonianos y la simulación de sistemas complejos.

En este repositorio web se encuentra:

* Software (Instalaciones necesarias para el curso)
* Ejercicios (Ejercicios y tareas)
* Clases (Presentaciones de la clase)

## Temáticas de los cursos:

## SEMANA DEL 23 AL 27 DE NOVIEMBRE

###Integrabilidad en sistemas Hamiltonianos
18:15 a 19:45

Alexander Arredondo, Ph.D.

Programa de Matemáticas Fundación Universitaria Konrad Lorenz, Bogotá

<Alexander.Arredondo@konradlorenz.edu.co>

Temario:

*   Sistemas hamiltonianos    
*   Integrabilidad 
*   Teoremas Clásicos
* Integrabilidad de campos hamiltonianos polinomiales

### Modelado integrativo y simulación de sistemas complejos
20:00 a 21:30

Manuel Balaguera, Ph.D.

Programa de Matemáticas
Fundación Universitaria Konrad Lorenz, Bogotá 

<manueli.balagueraj@konradlorenz.edu.co>

Temario:

* Bases conceptuales de la complejidad y los sistemas complejos
* Investigación de operaciones y computación científica para el tratamiento de problemas complejos.
* Paradigmas y recursos actuales para el análisis de sistemas complejos, para su modelado y su simulación.
* La visualización científica como recurso fundamental en la comprensión, el análisis y el tratamiento de problemas complejos.
* Perspectivas y oportunidades contemporáneas y locales para el uso del modelado integrativo y la simulación en problemas de alto impacto.    

## SEMANA DEL 30 DE NOVIEMBRE AL 4 DE DICIEMBRE 

###Introducción a la propagación de ondas sísmicas
18:15 a 19:45

Carlos Piedrahíta, Ph.D.

Instituto de Matemáticas
Universidad de Antioquia, Medellín

<cpiedrahita@udem.edu.co>

Temario:

* Conceptos Matemáticos Básicos (Tensores y Análisis en Varias Variables)
* Conceptos de Mecánica del Continuo (Esfuerzos y Deformaciones)
* Propagación de Ondas en un Medio Elástico
* Solución de la Ecuación utilizando Ondas Planas y Teoría de Rayos
* Propagación  de Ondas a través de Discontinuidades (Coeficientes de Reflexión y Transmisión)
* Uso de herramientas computacionales, en particular el CSHOT, librería del SU.

###Grafos para el análisis de imágenes y redes sociales
20:00 a 21:30

Santiago Velasco, Ph.D.

Departamento de Matemáticas y Sistemas
Mines ParisTech, París, Francia


<velasco@cmm.ensmp.fr>

Temario:

* Nociones básicas de grafos
* Algoritmos en grafos
* Filtrado en grafos
* Detección de comunidades


## Instrucciones del material:

Para utilizar el material del curso recomiendo:

* Descargar todo el material:
Al lado izquierdo en la página del repositorio aparecer un ícono que permite descargar todo el material
("Downloads") del repositorio a su computador.

* Clonar el Repositorio:
Al lado izquierdo en la página del repositorio aparecer un ícono que permite Clonar ("Clone") 
el repositorio a su computador.

Al realizar cualquiera de estas acciones puede abrir el contenido fácilmente,i.e., .HTML (Con un explorador)
.ipynb (con una terminal)

Durante el curso se explicará cómo mantener el repositorio actualizado. Las referencias están en cada documento.

# Instalar Python y Ubuntu

En el siguiente enlace encontrarán toda la información necesaria para instalar Ubuntu y las librerías más usadas de Python:


<https://bitbucket.org/alejandroc137/cursopython>


==============================================

#### Contacto

Alejandro Cárdenas-Avendaño

Programa de Matemática

<alejandro.cardenasa@konradlorenz.edu.co>

#### Curso

<http://mi.konradlorenz.edu.co/curso-sistemas-dinamicos/>
