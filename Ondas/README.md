#Introducción a la propagación de ondas sísmicas
Carlos Piedrahíta, Ph.D.

Instituto de Matemáticas
Universidad de Antioquia, Medellín

<cpiedrahita@udem.edu.co>

## Software:
Seismic Unix, que se pueden instalar en LINUX (cualquier versión Ubuntu, Debian,etc.).

Última versión SU 43R8 Full Source Code (current release):

<http://www.cwp.mines.edu/cwpcodes/>

### Instrucciones:

Las instrucciones completas, en inglés, están en un archivo llamado "Installation_Instructions". Más información se puede encontrar en <http://www.taringa.net/posts/linux/12096482/Como-instalar-Seismic-Unix-SU-en-Ubuntu-11-04.html>

1. Descargar la última versión:            
    <http://www.cwp.mines.edu/cwpcodes/>
2. Modificar el PATH y la variable de entorno así:

    Desde la terminal digitar

            nano ~/.bashrc
    
    Agregar al final las siguientes dos líneas, suponiendo  que se descomprimió en /home/fukl/Escritorio/Seismic:

            export CWPROOT=/home/fukl/Escritorio/Seismic 
            export PATH="$PATH:$CWPROOT/bin" 

3. Buscar en el directorio donde están los archivos, i.e.,

    *.../src/configs* 
    
    un archivo que se llama Makefile.config.ARCH donde ARCH es la arquitectura del sistema operativo (desde la terminal se puede conocer con el comando *uname -m*)
    Luego digitar:

            cd $CWPROOT/src
            cp configs/Makefile.config.ARCH
            $CWPROOT/src/Makefile.config

4. Instalar Seismic Unix (SU)

    - Requerimientos:

            sudo apt-get install libx11-dev
            sudo apt-get install libxt-dev

    Luego sí se puede instalar digitando:

            make install
            make xtinstall

5. Se puede comprobar que la instalación se realizó con éxito digitando en la terminal:

            suvibro
    
## Libros

###Teoría de Rayos

- <http://www.cpgg.ufba.br/publicacoes/popov.pdf >

### Propagación de Ondas Sísmicas
- <http://www.freebookcentre.net/physics-books-download/A-Short-Course-on-Theoretical-Seismology-[Paul-G.-Richards-PDF-106p].html>