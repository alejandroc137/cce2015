# Modelado integrativo y simulación de sistemas complejos
Manuel Balaguera, Ph.D.

Programa de Matemáticas
Fundación Universitaria Konrad Lorenz, Bogotá
 
<manueli.balagueraj@konradlorenz.edu.co>

## Paradigmas de Modelado

<https://drive.google.com/folderview?id=0B5n_eXTTYKl6YkhfVWl3TmVKSzQ&usp=sharing>

Encontrarán los instaladores de 

(1) VENSIM, 

(2) OpenModelica, 

(3) NetLogo, 

(4) PTOLEMY.

Los cuales son aplicativos pequeños para Windows y cada uno corresponde a un diferente paradigma de modelado.

 