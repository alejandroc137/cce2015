import csv
import time as time
import numpy as np
from pylab import *
from scipy.spatial.distance import pdist,squareform

#Tutorial sobre los graphos de ciudades en Colombia
#Implementado por Santiago Velasco-Forero
#email: velasco@cmm.ensmp.fr

##########################################################
print("Tutorial de utilizacion de arboles en imagenes\n")
print("por Santiago Velasco-Forero\n")
print("email: velasco@cmm.ensmp.fr\n")
print("http://cmm.ensmp.fr/~velasco\n")
##########################################################

results = []
st = time.time()
print("Decargando la informacion sobre las ciudades de Colombia")
with open('/users/Santiago/Desktop/GraphCourse/CiudadesColombia.csv') as inputfile:
    for row in csv.reader(inputfile,lineterminator='\n',delimiter=';'):
        results.append(row)
print ('in ' +str(time.time() - st) + ' ms\n')

print("Visualizando el grafo de ciudades")
xi=np.zeros((len(results),2))
regions=np.zeros((len(results),1))
for i in range(0,len(results)):
    v=results[i]
    xi[i,0]=v[3]
    xi[i,1]=v[4]
    regions[i]=v[1]
print 

scatter(xi[:,1], xi[:,0],c=regions)
xlabel('Latitud')
ylabel('Longitud')
title('Ciudades de Colombia')
grid(True)

xi=xi[nonzero(regions>0)[0],:]
dist = squareform(pdist(xi, metric="euclidean", p=2))   
indexi=nonzero(dist<.2)
pin= indexi[0]
pout= indexi[1]
for i in range(0,len(pin)):
    if pin[i]>pout[i]:
        pointin=xi[pin[i],:]
        pointout=xi[pout[i],:]
        plot((pointin[1],pointout[1]),(pointin[0],pointout[0]),'r-')
show()



